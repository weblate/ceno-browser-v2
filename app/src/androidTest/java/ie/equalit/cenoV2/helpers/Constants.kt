package ie.equalit.cenoV2.helpers

object Constants {
    const val LONG_CLICK_DURATION: Long = 5000
}
